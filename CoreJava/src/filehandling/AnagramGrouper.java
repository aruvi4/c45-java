package filehandling;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class AnagramGrouper {
	
	public static String sorted(String word) {
		char[] letters = word.toCharArray();
		Arrays.sort(letters);
		return String.valueOf(letters);
	}
	
	public static boolean areAnagrams(String word1, String word2) {
		return sorted(word1).equals(sorted(word2));
	}
	
	public static List<List<String>> groupAnagramsInefficient(List<String> words) {
		List<List<String>> groups = new ArrayList<List<String>>();
		for (int i = 0; i < words.size(); i++) {
			//consider words.get(i)
			List<String> allAnagramsOfIthWord = new ArrayList<String>();
			allAnagramsOfIthWord.add(words.get(i));
			for (int j = i + 1; j < words.size(); j++) {
				if (areAnagrams(words.get(i), words.get(j)))
					allAnagramsOfIthWord.add(words.get(j));
			}
			groups.add(allAnagramsOfIthWord);
		}
		return groups;
	}
	
	public static List<List<String>> groupAnagrams(List<String> words) {
		List<List<String>> anagrams = new ArrayList<List<String>>();
		Map<String, List<String>> groups = new HashMap<String, List<String>>();
		for (String word : words) {
			char[] letters = word.toCharArray();
			Arrays.sort(letters);
			String sortedWord = String.valueOf(letters);
			if (!(groups.containsKey(sortedWord)))
				groups.put(sortedWord, new ArrayList<String>());
			groups.get(sortedWord).add(word);
		}
		for (String key : groups.keySet())
			anagrams.add(groups.get(key));
		return anagrams;
	}
	
	public static List<String> getListFromFile(String filename) throws IOException {
		Path file = Paths.get(filename);
		Scanner sc = new Scanner(file);
		List<String> words = new ArrayList<String>();
		while(sc.hasNext())
			words.add(sc.nextLine());
		sc.close();
		return words;
	}
	
	public static void writeToFile(String filename, List<List<String>> groups) throws IOException {
		Path file = Paths.get(filename);
		if (!Files.exists(file))
			Files.createFile(file);
		BufferedWriter writer = Files.newBufferedWriter(file, StandardOpenOption.WRITE);
		for (List<String> group : groups) {
			for (String word : group) {
				writer.write(word + ", ");
			}
			writer.write("\n");
		}
		writer.close();
	}
	
	public static void main(String[] args) {
		try {
			List<String> words = getListFromFile("/home/aruvi/Desktop/newfile");
			writeToFile("/home/aruvi/Desktop/grouped_anagrams.txt", groupAnagrams(words));
		} catch (IOException e) {
			
			e.printStackTrace();
		}
			
	}

}
