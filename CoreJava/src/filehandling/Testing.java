package filehandling;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Scanner;

public class Testing {
	
	public static int countVowels(String line) {
		int count = 0;
		String vowels = "aeiouAEIOU";
		for (char c : line.toCharArray()) {
			if (vowels.contains(String.valueOf(c)))
				count++;
		}
		return count;
	}
	
	public static int countConsonants(String line) {
		int count = 0;
		String consonants = "bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ";
		for (char c : line.toCharArray()) {
			if (consonants.contains(String.valueOf(c)))
				count++;
		}
		return count;
	}
	
	public static int[] countVowelsAndConsonants(String filePath) throws IOException {
		Path file = Paths.get(filePath);
		Scanner sc = new Scanner(file);
		int numVowels = 0;
		int numConsonants = 0;
		while(sc.hasNext()) {
			String line = sc.nextLine();
			numVowels += countVowels(line);
			numConsonants += countConsonants(line);
		}
		int[] arr = new int[2];
		arr[0] = numVowels;
		arr[1] = numConsonants;
		sc.close();
		return arr;
	}
	
	public static void main(String[] args) {
		//Path file = Paths.get("/home/aruvi/c45/CoreJava/src/filehandling/Testing.java");
		//Path file = Paths.get("C:/Users/username/OneDrive/Desktop/filename");
		try {
			//Files.createFile(file);
//			Scanner sc = new Scanner(file);
//			
//			while(sc.hasNext()) {
//				sc.useDelimiter("");
//				String c = sc.next();
//				System.out.println(c);
//			}
			
			System.out.println(Arrays.toString(countVowelsAndConsonants("/home/aruvi/Desktop/newfile")));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
