package cardgames;

import java.util.Random;

public class Game {
	
	public static void main(String[] args) {
		System.out.println(Card.SPADES);
		try {
			Card c = new Card(Card.SPADES, Card.EIGHT);
			System.out.println("this is the rest of my program");
			System.out.println(c);
			System.out.println("I am going to do other things");
		} catch (InvalidSuitException | InvalidRankException e) {
			e.printStackTrace();
		} catch (ArithmeticException e) {
			System.out.println("I have caught an arithmetic exception");
			e.printStackTrace();
		}
		
		try {
			Card c1 = new Card(Card.CLUBS, Card.TEN);
			Card c2 = new Card(Card.CLUBS, Card.JACK);
			Card c3 = new Card(Card.SPADES, Card.KING);
			Card c4 = new Card(Card.SPADES, Card.TWO);
			Hand h = new Hand();
			h.add(c1);
			h.add(c2);
			h.add(c3);
			h.add(c4);
			h.sort();
			System.out.println(h);
		} catch(InvalidSuitException | InvalidRankException e) {
			e.printStackTrace();
		}
		
	}

}
