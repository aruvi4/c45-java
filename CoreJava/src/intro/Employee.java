package intro;

public class Employee {
	
	private int employeeId;
	private double salary;
	private String name;
	
	public Employee() {
		employeeId = 0;
		salary = 0;
		name = "";
	}
	
	public Employee (int firstEmployeeId, double firstSalary, String firstName) {
		employeeId = firstEmployeeId;
		salary = firstSalary;
		name = firstName;
	}
	
	public void giveIncrement(double increment) {
		salary += increment;
	}
	
	public void setName(String newName) {
		name = newName;
	}
	
	public String toString() {
		return "Employee id: " + employeeId + " name: " + name + " salary: " + salary;
	}
	
	public String someStringRepresentation() {
		return "This is an alternate representation of this employee " + employeeId;
	}

}

