package intro;

public class Fraction {
	
	private int nr;
	private int dr;
	
	public Fraction() {
		nr = 1;
		dr = 1;
	}
	
	public Fraction(int nr, int dr) {
		if (dr == 0) {
			System.out.println("Denominator can't be zero, setting to 1");
			this.dr = 1;
		}
		else
			this.dr = dr;
		this.nr = nr;
		reduce();
	}
	
	public Fraction(int num) {
		this.nr = num;
		this.dr = 1;
	}
	
	public Fraction add(Fraction other) {
		int sumDr = other.dr * this.dr;
		int sumNr = other.nr * this.dr + this.nr * other.dr;
		return new Fraction(sumNr, sumDr);
	}
	
	public void addInPlace(Fraction other) {
		this.dr = other.dr * this.dr;
		this.nr = other.nr * this.dr + this.nr * other.dr;
	}
	
	public Fraction subtract(Fraction other) {
		return add(new Fraction(-other.nr, other.dr));
	}
	
	public Fraction multiply(Fraction other) {
		return new Fraction(this.nr * other.nr, this.dr * other.dr);
	}
	
	public Fraction reciprocal() {
		return new Fraction(this.dr, this.nr);
	}
	
	public Fraction divide(Fraction other) {
		return multiply(other.reciprocal());
	}
	
	public Fraction pow(int exp) {
		return new Fraction((int)Math.pow(nr, exp), (int)Math.pow(dr, exp));
	}
	
	private int gcd(int a, int b) {
		if (a % b == 0)
			return b;
		return gcd(b, a % b);
	}
	
	public void reduce() {
		int gcd = gcd(nr, dr);
		nr /= gcd;
		dr /= gcd;
	}
	
	public String toString() {
		return nr + "/" + dr;
	}

}
