package intro;

public class WholeNumberException extends Exception{
	
	public WholeNumberException(String message) {
		super(message);
	}
	
}
