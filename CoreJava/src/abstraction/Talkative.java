package abstraction;

public interface Talkative {
	
	public String talk();

}
