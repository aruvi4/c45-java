package abstraction;

public class Cat extends Animal{
	
	public Cat() {
		super("Felis catus");
	}
	
	@Override
	public String talk() {
		return "I am " + species + "and I say meow! ";
	}

}
