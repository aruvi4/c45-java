package chess;

public abstract class Piece {
	
	public static final boolean WHITE = true;
	public static final boolean BLACK = false;
	
	public static final String ROOK = "Rook";//Elephant
	public static final String BISHOP = "Bishop";//Camel
	public static final String KNIGHT = "Knight";//Horse
	public static final String KING = "King";//King
	public static final String QUEEN = "Queen";//Queen
	public static final String PAWN = "Pawn";//Soldier
	
	public String type;
	public boolean colour;
	public Position position;
	
	public Piece(String type, boolean colour, Position position) {
		this.type = type;
		this.colour = colour;
		this.position = position;
	}
	
	private int getAbsRowDiff(Position next) {
		return Math.abs(this.position.getRow() - next.getRow());
	}
	
	private int getAbsColumnDiff(Position next) {
		return Math.abs(this.position.getColumn() - next.getColumn());
	}
	
	private boolean canMoveRook(Position next) {
		return next.getRow() == this.position.getRow() || 
				next.getColumn() == this.position.getColumn();
	}
	
	private boolean canMoveBishop(Position next) {
		return getAbsRowDiff(next) ==
				getAbsColumnDiff(next);
	}
	
//	private boolean canMoveKing(Position next) {
//		return getAbsRowDiff(next) <= 1 &&
//				getAbsColumnDiff(next) <= 1;
//	}
	
	private boolean canMoveKnight(Position next) {
		return (getAbsRowDiff(next) == 2 &&
				getAbsColumnDiff(next) == 1)
				||
				(getAbsColumnDiff(next) == 1 &&
				 getAbsRowDiff(next) == 2);
	}
	
	/*
	 * Pass a startRow to this method, should be 2 or 7
	 */
	public boolean canMovePawn(Position next, int startRow) {
		if (this.position.getColumn() != next.getColumn())
			return false;//sort of wrong, pawn can move columns if there is a kill opportunity
		if (startRow == 2) {
			if (this.position.getRow() == 2)
				return next.getRow() - this.position.getRow() == 1 || 
					next.getRow() - this.position.getRow() == 2;
			return next.getRow() == this.position.getRow() + 1;
		}
		if (startRow == 7)
			return next.getRow() == this.position.getRow() - 1;
		return false;
	}
	
	/*
	 * Assuming white starts at rows 1, 2 and black starts at rows 7, 8
	 */
	public boolean canMovePawn(Position next) {
		if (colour == Piece.WHITE)
			return canMovePawn(next, 2);
		else
			return canMovePawn(next, 7);
	}
	
	/*
	 * Assuming that next does not contain another piece of the same colour
	 */
	public boolean canMove(Position next) {
		switch(type) {
		case(Piece.ROOK):
			return canMoveRook(next);
		case(Piece.BISHOP):
			return canMoveBishop(next);
		case(Piece.QUEEN):
			return canMoveRook(next) || canMoveBishop(next);
//		case(Piece.KING):
//			return canMoveKing(next);
		//refer to King::canMove()
		case(Piece.KNIGHT):
			return canMoveKnight(next);
		case(Piece.PAWN):
			return canMovePawn(next);
		default:
			return false;
		}
	}

}
