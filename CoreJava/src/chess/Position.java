package chess;

public class Position {
	
	int row;
	int column;
	
	public static int getAbsRowDiff(Position a, Position b) {
		return Math.abs(a.getRow() - b.getRow());
	}
	
	public static int getAbsColumnDiff(Position a, Position b) {
		return Math.abs(a.getColumn() - b.getColumn());
	}
	
	public Position(int row, int column) {
		this.row = row;
		this.column = column;
	}
	
	public int getRow() {
		return row;
	}
	
	public int getColumn() {
		return column;
	}
}
