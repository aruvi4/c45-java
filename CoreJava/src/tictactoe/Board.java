package tictactoe;

import java.util.HashSet;
import java.util.Set;

public class Board {
	
	private String[][] cells;
	
	public static final String X = "X";
	public static final String O = "O";
	public static final String blank = "_";
	
	public Board() {
		cells = new String[3][3];
		for(int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				cells[i][j] = blank;
	}
	
	public boolean mark(int row, int column, String value) {
		if (cells[row][column] == blank) {
			cells[row][column] = value;
			return true;
		}
		else
			return false;
	}
	
	public boolean isDraw() {
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				if (cells[i][j].equals(blank))
					return false;
		return true;
	}

	private boolean isUniform(String[] vals) {
		Set<String> valSet = new HashSet<String>();
		for (String v : vals) 
			valSet.add(v);
		return valSet.size() == 1 && !valSet.contains(blank);
	}
	
	private boolean isWonRow(int row) {
		return isUniform(cells[row]);
	}
	
	private boolean isWonAnyRow() {
		for (int i = 0; i < 3; i++)
			if(isWonRow(i))
				return true;
		return false;
	}
	
	private boolean isWonColumn(int column) {
		String[] cellsInColumn = {cells[0][column], cells[1][column], cells[2][column]};
		return isUniform(cellsInColumn);
	}
	
	private boolean isWonAnyColumn() {
		for (int i = 0; i < 3; i++)
			if(isWonColumn(i))
				return true;
		return false;
	}
	
	private boolean isWonAnyDiagonal() {
		String[] negativeDiagonal = {cells[0][0], cells[1][1], cells[2][2]};
		String[] positiveDiagonal = {cells[2][0], cells[1][1], cells[0][2]};
		return isUniform(negativeDiagonal) || isUniform(positiveDiagonal);
	}
	
	public boolean isWon() {
		return isWonAnyRow() || isWonAnyColumn() || isWonAnyDiagonal();
	}
	
	@Override
	public String toString() {
		String output = "";
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				output += "|" + cells[i][j];
			}
			output += "|\n";
		}
		return output;
	}

}
