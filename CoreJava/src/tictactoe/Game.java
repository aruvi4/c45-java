package tictactoe;

import java.util.Scanner;

public class Game {
	
	public static final boolean PLAYER_X = true;
	public static final boolean PLAYER_O = false;
	
	public static String value(boolean player) {
		return player ? Board.X : Board.O;
	}
	
	public static void main(String[] args) {
		Board b = new Board();
		System.out.println(b);
		Scanner sc = new Scanner(System.in);
		int row, column;
		boolean currentPlayer = PLAYER_X;
		while(true) {
			do {
				System.out.println("The current player is " +
						value(currentPlayer));
				System.out.println("Enter row 0-2 to place ");
				row = sc.nextInt();
				System.out.println("Enter column 0-2 to place ");
				column = sc.nextInt();
			
			} while(!b.mark(row, column, value(currentPlayer)));//what if this is an already marked space
			System.out.println("The board after this operation looks like: ");
			System.out.println(b);
			if (b.isWon()) {
				System.out.println(value(currentPlayer) + " has won!");
				break;
			}
			if (b.isDraw()) {
				System.out.println("It's a draw, run again to play again");
				break;
			}
			currentPlayer = !currentPlayer;//what if draw?
		}
	}

}
