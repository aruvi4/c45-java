package office;

public class FullTimeEmployee extends Employee{
	
	private double basicSalary;
	
	public FullTimeEmployee(String name, double basicSalary) {
		super(name, 'A');
		this.basicSalary = basicSalary;
	}
	
	@Override
	public double getDailySalary() {
		return basicSalary * 2 / 30;
	}

}
