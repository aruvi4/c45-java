package student;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Classroom {
	
	private List<Student> students;
	
	public Classroom(String filePath, String delimiter) throws IOException{
		Path file = Paths.get(filePath);
		Scanner sc = new Scanner(file);
		students = new ArrayList<Student>();
		
		while(sc.hasNext()) {
			Student temp = new Student(sc.nextLine(), delimiter);
			students.add(temp);
		}
	}
	
	public Classroom() {
		students = new ArrayList<>();
	}
	
	public void add(Student s) {
		students.add(s);
	}
	
	public void remove(Student s) {
		students.remove(s);
	}
	
	public double mean() {
		double total = 0;
		for (Student s : students)
			total += s.getScore();
		return total / students.size();
	}
	
	public double median() {
		List<Student> temp = new ArrayList<Student>(students);
		Collections.sort(temp);
		int len = temp.size();
		if (len % 2 != 0)
			return temp.get(len / 2).getScore();
		else {
			int preMedian = temp.get(len / 2 - 1).getScore();
			int postMedian = temp.get(len / 2).getScore();
			return (preMedian + postMedian) / 2.0;
		}
	}
	
	public Student searchByRollNumber(int rollNumber) {
		for (Student s : students)
			if (s.getRollNumber() == rollNumber)
				return s;
		return null;
	}
	
	public String getRankList() {
		List<Student> temp = new ArrayList<Student>(students);
		Collections.sort(temp);
		String output = "";
		int rank = 1;
		for (Student s : temp) {
			output += rank++ + ", " + s.getName() + "\n";
		}
		return output;
	}
	
	public String getRankListWithDups() {
		List<Student> temp = new ArrayList<Student>(students);
		Collections.sort(temp);
		String output = "";
		int rank = 1;
		output += rank + ", " + temp.get(0).getName() + "\n";
		for (int i = 1; i < temp.size(); i++) {
			Student current = temp.get(i);
			Student prev = temp.get(i - 1);
			if (current.getScore() != prev.getScore()) {
				rank = i + 1;
			}
			output += rank + ", " + temp.get(i).getName() + "\n";
		}
		return output;
	}
	
	public String getRankListWithDupsUsingMap() {
		Map<Integer, List<Student>> scoreToStudents = new HashMap<Integer, List<Student>>();
		for (Student s : students) {
			if (!scoreToStudents.containsKey(s.getScore()))
				scoreToStudents.put(s.getScore(), new ArrayList<Student>());
			scoreToStudents.get(s.getScore()).add(s);
		}
		List<Integer> allScores = new ArrayList<Integer> (scoreToStudents.keySet());
		Collections.sort(allScores); Collections.reverse(allScores);
		String output = "";
		int rank = 1;
		for (Integer score : allScores) {
			int groupRank = rank;
			for (Student s : scoreToStudents.get(score)) {
				output += groupRank + ", " + s.getName() + "\n";
				rank++;
			}
		}
		return output;
	}
	
	@Override
	public String toString() {
		return students.toString();
	}

}
