package student;

public class Student implements Comparable<Student>{
	
	private int rollNumber;
	private String name;
	private int score;
	
	public Student (String line, String delimiter) {
		String[] components = line.split(delimiter);
		
		rollNumber = Integer.valueOf(components[0]);
		name = components[1];
		score = Integer.valueOf(components[2]);
	}
	
	public Student(int rollNumber, String name, int score) {
		this.rollNumber = rollNumber;
		this.name = name;
		this.score = score;
	}
	
	public int getRollNumber() {
		return rollNumber;
	}
	
	public String getName() {
		return name;
	}
	
	public int getScore() {
		return score;
	}

	@Override
	public int compareTo(Student arg0) {
		// TODO Auto-generated method stub
		return (this.score > arg0.score) ? -1 : 
			(this.score == arg0.score) ? 0 : 1;
	}
	
	@Override
	public String toString() {
		return rollNumber + ": " + name + ", " + score + "marks";
	}
	
	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Student))
			return false;
		Student stOther = (Student) other;
		return this.rollNumber == stOther.rollNumber;
	}

}
